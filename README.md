# ThalesVinkler-SafeMilk

The main objective of this project is the development SafeMilk - a Web-based information system with mobile devices - to perform real-time access and registration of formaldehyde adulteration in the milk production chain.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

```
Pyhton 3.6.2
```
```
virtualenv
```
```
pip
```

### Installing

Create a virtual enviroment with virtualenv

```
virtualenv ENV
```

Activate the enviroment

```
source /path/to/ENV/bin/activate
```

Go to bin folder

```
cd bin
```

Add the project source

```
heroku git:clone -a safemilk
```

Go to project folder

```
cd safemilk
```

Start the remote server

```
pyhton manage.py runserver
```

### Folder Structure

    .safemilk
    ├── admin/              # Django Admin app(modulo)
    ├── colortest/          # App(modulo) de testes colorimetricos
    ├── logistic/           # App(modulo) de logistica de transporte
    ├── media/              # Diretorio de medias
    ├── static/             # Diretorio de plugins e componentes
    ├── app.json
    ├── manage.py
    ├── Procfile
    ├── requirements.txt
    └── runtime.txt 

## Deployment

1. Log in

```
$ heroku login
```

2. Clone the repository

```
$ heroku git:clone -a safemilk
```
```
$ cd safemilk
```

3. Deploy your changes

```
$ git add .
```
```
$ git commit -am "make it better"
```
```
$ git push heroku master
```

## Built With

* [Django](https://www.djangoproject.com/) - Python Web Framerowk
* [Django REST framework](https://maven.apache.org/) - Toolkit para construção de Web APIs


## Autor

* **Thales Vinkler** - *Web and mobile development* -*
